package com.realdolmen.noteapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun submit(view: View){
        if(txt_title.text.isNullOrBlank() || txt_content.text.isNullOrEmpty()) {
            Toast.makeText(this,R.string.TOAST_STRING,Toast.LENGTH_LONG).show()
        }else {
            txt_title_result.text = txt_title.text
            txt_content_result.text = txt_content.text
            resetEditTexts()
        }
    }

    fun newNote(view: View){
        resetEditTexts()
        txt_title_result.setText("")
        txt_content_result.setText("")
    }

    private fun resetEditTexts(){
        txt_title.setText("")
        txt_content.setText("")
    }

    fun reset(view:View){
        resetEditTexts()
    }
}
